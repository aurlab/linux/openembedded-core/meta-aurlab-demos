SUMMARY = "AURlab Embedded Linux Console"
SUMMARY_append_apalis-tk1-mainline = " (Mainline)"
DESCRIPTION = "AURlab console image with pps support and extra UARTs"

LICENSE = "MIT"

inherit core-image

#start of the resulting deployable tarball name
export IMAGE_BASENAME = "Console-Image"
MACHINE_NAME ?= "${MACHINE}"
IMAGE_NAME = "${MACHINE_NAME}_${IMAGE_BASENAME}"

SYSTEMD_DEFAULT_TARGET = "graphical.target"

IMAGE_FEATURES += " \
    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', '', \
       bb.utils.contains('DISTRO_FEATURES',     'x11', 'x11', \
                                                       '', d), d)} \
"

IMAGE_LINGUAS = "en-us"
#IMAGE_LINGUAS = "de-de fr-fr en-gb en-us pt-br es-es kn-in ml-in ta-in"
#ROOTFS_POSTPROCESS_COMMAND += 'install_linguas; '

ROOTFS_PKGMANAGE_PKGS ?= '${@oe.utils.conditional("ONLINE_PACKAGE_MANAGEMENT", "none", "", "${ROOTFS_PKGMANAGE}", d)}'

CONMANPKGS ?= "connman connman-plugin-loopback connman-plugin-ethernet connman-plugin-wifi connman-client"

IMAGE_INSTALL += " \
    packagegroup-boot \
    packagegroup-basic \
    udev-extra-rules \
    ${CONMANPKGS} \
    ${ROOTFS_PKGMANAGE_PKGS} \
    timestamp-service \
    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', \
                         'weston weston-init wayland-terminal-launch', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'x11 wayland', \
                         'weston-xwayland xterm', \
       bb.utils.contains('DISTRO_FEATURES', 'x11', \
                         'x-window-xterm', '', d), d)} \
    \
    nano \
    pps-tools \
    screen \
    grep \
    tar \
    bzip2 \
    glibc-staticdev \
"

require recipes-images/images/tdx-extra.inc

IMAGE_DEV_MANAGER   = "udev"
IMAGE_INIT_MANAGER  = "systemd"
IMAGE_INITSCRIPTS   = " "
IMAGE_LOGIN_MANAGER = "busybox shadow"

# Include custom packages
#IMAGE_INSTALL_append = "nano"
#IMAGE_INSTALL_append += "pps-tools"
#IMAGE_INSTALL_append += "screen"
#IMAGE_INSTALL_append += "grep"
#IMAGE_INSTALL_append += "tar"
#IMAGE_INSTALL_append += "glibc-staticdev"
